package com.moviles.workapp.model

import android.app.Activity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.moviles.workapp.presenter.AuthenticationPresenter
import com.moviles.workapp.presenter.userPresenter

class AuthenticatorInteractorImpl(var authPresenter: AuthenticationPresenter): AuthenticatorInteractor {
    private var authenticatorRepository:AuthenticatorRepository = AuthenticatorRepositoryImpl(authPresenter)

    override fun login(email: String, pass: String) {
        authenticatorRepository.login(email, pass)
    }

    override fun loginGoogle(task: Task<GoogleSignInAccount>) {
        authenticatorRepository.loginGoogle( task)
    }

    override fun signup(email: String, pass: String) {
        authenticatorRepository.signup(email, pass)
    }

    override fun verifyLogin():String {
        return authenticatorRepository.verifyLogin()
    }

    override fun logout() {
         authenticatorRepository.logout()
    }

}