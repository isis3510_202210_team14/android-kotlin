package com.moviles.workapp.model

public data class User(var email:String?=null, val name:String?=null, val age:Int?=null, val studyLevel:String?=null, val phone:String?=null, val location:String?=null, val skills:List<String>?=null, val configuration:Int) {

}