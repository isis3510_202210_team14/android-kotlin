package com.moviles.workapp.view

import android.app.Activity
import android.content.Context

interface AuthenticationManager {
    fun login()

    fun loginGoogle()

    fun signup(email: String, pass: String)

    fun verifyLogin()

    fun exitoso(exito: Boolean, error: String?)

    fun getContext(): Context

    fun setLoading(status: Boolean)

}