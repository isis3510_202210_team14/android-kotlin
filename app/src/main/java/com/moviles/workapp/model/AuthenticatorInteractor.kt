package com.moviles.workapp.model

import android.app.Activity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task

interface AuthenticatorInteractor {
    fun login(email: String, pass: String)

    fun loginGoogle(task: Task<GoogleSignInAccount>)

    fun signup(email: String, pass: String)

    fun verifyLogin():String

    fun logout()
}