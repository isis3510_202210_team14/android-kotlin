package com.moviles.workapp.view

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.Snackbar
import com.moviles.workapp.R
import com.moviles.workapp.databinding.ActivityJobDetailBinding
class JobDetail : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityJobDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityJobDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val name = intent.getStringExtra("name")
        val description = intent.getStringExtra("description")
        val salary = intent.getIntExtra("salary", 0)
        val time = intent.getStringExtra("time")
        val image = intent.getIntExtra("image", R.drawable.workapp_logo)

        findViewById<ImageView>(R.id.profile).setImageResource(image)
        findViewById<TextView>(R.id.companyText).text = name
        findViewById<TextView>(R.id.DaysText).text = time
        findViewById<TextView>(R.id.SalaryText).text = salary.toString()


    }
}