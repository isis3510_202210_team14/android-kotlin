package com.moviles.workapp.model


import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.moviles.workapp.presenter.AuthenticationPresenter

class AuthenticatorRepositoryImpl(private var authPresenter: AuthenticationPresenter): AuthenticatorRepository {

    private val email = "userEmail"
    private val name = "userName"
    private val age = "userAge"
    private val studyLevel = "userStudyLevel"
    private val phone = "userPhone"
    private val location = "userLocation"
    private val skills = "userSkill"
    private val configuration = "Configuracion"

    private val context = authPresenter.getContext()
    private var fireAuth = FirebaseAuth.getInstance()
    override fun login(email: String, pass: String) {
        fireAuth.signInWithEmailAndPassword(email,pass)
            .addOnCompleteListener{ task ->
                if (task.isSuccessful) {
                    val uid = fireAuth.currentUser?.uid
                    getUserFromFirebase(uid?:"")
                } else {
                    // If sign in fails, display a message to the user.
                    authPresenter.exitoso(false, task.exception?.localizedMessage)
                }
            }
    }


    private fun getUserFromFirebase(uid: String){
        var user: User? = null
        val database = FirebaseDatabase.getInstance().getReference("Users")
        val a = database.child(uid).get().addOnSuccessListener {

            val child = it.child("skills")
            if (child.value != null){
                val skills: List<String> = it.child("skills").value as List<String>
                val phone: String = it.child("phone").value as String
                val name: String = it.child("name").value as String
                val loc: String = it.child("location").value as String
                val study: String = it.child("studyLevel").value as String
                val age: Long = it.child("age").value as Long
                val configuracion: Long = it.child("configuration").value as Long
                val email: String = it.child("email").value as String

                user = User(email, name, age.toInt(), study, phone, loc, skills, configuracion.toInt())
                storeUserInPreferences(user!!)
            }
            authPresenter.exitoso(true, (user?.configuration?:0).toString())
        }.addOnFailureListener{
            print(it.localizedMessage)
            authPresenter.exitoso(true, it.localizedMessage)
        }
    }

    fun storeUserInPreferences(user: User){
        val sharedPrefs = context.getSharedPreferences("userFile", Context.MODE_PRIVATE)
        with(sharedPrefs.edit()) {
            //Aniado los valores necesarios
            putString(email, user.email)
            putString(name, user.name)
            putInt(age, user.age ?: 0)
            putString(studyLevel, user.studyLevel)
            putString(phone, user.phone)
            putString(location, user.location)

            var i = 0
            var skill = ""

            while (i < user.skills?.size ?: 0) {
                skill = user.skills?.get(i) ?: ""
                putString(skills + (i++), skill)
            }
            apply()
        }
    }

    override fun loginGoogle(task: Task<GoogleSignInAccount>) {
        try {
            val account = task.getResult(ApiException::class.java)

            if (account != null) {

                val credential = GoogleAuthProvider.getCredential(account.idToken, null)

                FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            val uid = fireAuth.currentUser?.uid
                            getUserFromFirebase(uid?:"")
                        } else {
                            authPresenter.exitoso(false, it.exception?.localizedMessage)
                        }
                    }

            }
        }catch (e: ApiException){
            authPresenter.exitoso(false, e.localizedMessage)
        }

    }

    override fun signup(email: String, pass: String) {
        fireAuth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener{ task ->
                if (task.isSuccessful) {

                    authPresenter.exitoso(true, null)
                } else {
                    authPresenter.exitoso(false, task.exception?.localizedMessage)
                }
            }
    }

    override fun logout(){
        fireAuth.signOut()
        val sharedPrefs = context.getSharedPreferences("userFile", Context.MODE_PRIVATE)
        with(sharedPrefs.edit()){
            putString(email, "")
            putString(name, "")
            putInt(age, -1)
            putString(studyLevel, "")
            putString(phone, "")
            putString(location, "")
            putInt(configuration, 0)

            var i = 0
            while(i<10){
                putString(skills+(i++), "")
            }
            apply()
        }
    }

    override fun verifyLogin():String {
        val currentUser = fireAuth.currentUser
        if (currentUser != null){
            val sharedPrefs = context.getSharedPreferences("userFile", Context.MODE_PRIVATE)
            val confStep = sharedPrefs.getInt("Configuracion", 0)
            return confStep.toString()
        }
        return "-1"
    }

}