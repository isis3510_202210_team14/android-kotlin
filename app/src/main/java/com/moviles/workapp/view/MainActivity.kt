package com.moviles.workapp.view

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.moviles.workapp.R
import com.moviles.workapp.connectivityManager.ConnectionLiveData
import com.moviles.workapp.presenter.AuthenticationPresenter
import com.moviles.workapp.presenter.AuthenticationPresenterImpl


class MainActivity : AppCompatActivity(), AuthenticationManager {
    private var GOOGLE_SIGN_IN = 100


    //Connection with the user presenter
    private var authPresenter: AuthenticationPresenter? = null

    //Variables for the buttons and texts
    private lateinit var emailText: EditText
    private lateinit var passText: EditText

    private lateinit  var logInButton: Button
    private lateinit  var googleButton: Button

    private lateinit var loading: ProgressBar

    private lateinit var connectivityLiveData: ConnectionLiveData

    private var internetConn = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        //drawAlert()



        authPresenter= AuthenticationPresenterImpl(this)

        setup()

        verifyLogin()

        ConnectionLiveData(this@MainActivity).observe(this) {
            if (it) {
                internetConn = true
            } else {
                internetConn = false
                drawAlert()
            }
        }

        //Authentication

    }



    override fun onResume() {
        super.onResume()
        //drawAlert()
    }


    private fun drawAlert(){
        setLoading(true)
        object : CountDownTimer(8000, 1000) {
            // Callback function, fired on regular interval
            override fun onTick(millisUntilFinished: Long) {
                if(internetConn){
                    setLoading(false)
                    cancel()
                }
            }

            override fun onFinish() {
                setLoading(false)
                val dialog = Dialog(this@MainActivity)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.setCancelable(false)
                dialog.setContentView(R.layout.noconnection)

                dialog.findViewById<Button>(R.id.btnTryAgain).setOnClickListener {
                    if(internetConn){
                        dialog.dismiss()
                    }else{
                        dialog.dismiss()
                        drawAlert()
                    }
                }

                dialog.show()
            }
            }.start()
    }

    private fun setup() {
        val dontHaveAccount: TextView = findViewById(R.id.textView10)
        dontHaveAccount.setOnClickListener{
            val intent = Intent(this, Signup:: class.java)
            startActivity(intent)
        }

        emailText=findViewById<EditText>(R.id.editTextTextEmailAddress)
        passText=findViewById<EditText>(R.id.editTextTextPassword)

        title="Autenticacion"


        logInButton = findViewById<Button>(R.id.LoginButton)

        googleButton = findViewById<Button>(R.id.googleButton)

        loading = findViewById(R.id.loadBarMain)

        logInButton.setOnClickListener{
            login()
        }

        googleButton.setOnClickListener{
            loginGoogle()
        }
    }

    override fun setLoading(status: Boolean){
        if(status){
            loading.visibility = View.VISIBLE
            emailText.isEnabled = false
            passText.isEnabled = false

            logInButton.visibility = View.INVISIBLE
            googleButton.visibility = View.INVISIBLE




        }else{
            loading.visibility = View.INVISIBLE
            emailText.isEnabled = true
            passText.isEnabled = true

            logInButton.visibility = View.VISIBLE
            googleButton.visibility = View.VISIBLE
        }
    }

    //Waits the coroutines to have a response
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==GOOGLE_SIGN_IN){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            authPresenter?.loginGoogle(task)

        }
    }

    override fun login() {
        val email = emailText.text
        val pass = passText.text
        if (email.isNotEmpty() && pass.isNotEmpty()){

            authPresenter?.login(email.toString()?:"", pass.toString()?:"")

        }else{
            showErrorMessage("Introduzca valores de texto y contraseña")
        }
    }

    override fun loginGoogle() {
        val googleConf: GoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val googleClient:GoogleSignInClient=GoogleSignIn.getClient(this, googleConf)
        googleClient.signOut()

        startActivityForResult(googleClient.signInIntent, GOOGLE_SIGN_IN)
    }

    override fun signup(email: String, pass: String) {

    }

    override fun verifyLogin() {
        authPresenter?.verifyLogin()
    }

    override fun exitoso(exito: Boolean, error: String?) {
        if(exito){
            if (error == "0"){
                showCreateProfile1()
            }else if(error == "1"){
                showCreateProfile2()
            }else if(error == "2"){
                showCreateProfile3()
            }else if(error == "3"){
                showJobs()
            }
        }else{
            if(error != null){
                showErrorMessage(error?:"")
            }
        }
    }

    fun showErrorMessage(message: String){
        var builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(message)
        builder.setPositiveButton("Aceptar", null)
        var dialog:AlertDialog=builder.create()
        dialog.show()

    }

    private fun showCreateProfile1(){
        val createProfileIntent = Intent(this, CreateProfile1::class.java)
        startActivity(createProfileIntent)
    }

    private fun showCreateProfile2(){
        val createProfileIntent = Intent(this, CreateProfile2::class.java)
        startActivity(createProfileIntent)
    }

    private fun showCreateProfile3(){
        val createProfileIntent = Intent(this, CreateProfile3::class.java)
        startActivity(createProfileIntent)
    }

    fun showJobs(){
        val intent: Intent = Intent(this, FindJobs:: class.java)
        startActivity(intent)
    }

    override fun getContext():Context{
        return applicationContext
    }
}


