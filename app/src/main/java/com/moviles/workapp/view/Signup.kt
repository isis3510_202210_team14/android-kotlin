package com.moviles.workapp.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import androidx.appcompat.app.AlertDialog
import com.moviles.workapp.R
import com.moviles.workapp.presenter.AuthenticationPresenter
import com.moviles.workapp.presenter.AuthenticationPresenterImpl


class Signup : AppCompatActivity(), AuthenticationManager {

    //Definen los emails y las passwords del usuario
    private lateinit var email: EditText
    private lateinit var pass1: EditText
    private lateinit var pass2: EditText

    private lateinit var loading: ProgressBar

    //Connection with the user presenter
    private var authPresenter: AuthenticationPresenter? = null

    //Mensaje de error
    private lateinit var error: TextView

    //Definen los botones de inicio de sesion
    private lateinit var signin: Button

    override fun onStart() {
        super.onStart()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        supportActionBar?.hide()
        drawAlert()

        authPresenter= AuthenticationPresenterImpl(this)

        setup()
    }

    override fun onResume() {
        super.onResume()
        drawAlert()
    }

    private fun drawAlert(){
        if (checkForInternet(this)) {
            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            var builder = android.app.AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.noconnection,null)
            val button =view.findViewById<Button>(R.id.btnTryAgain)
            builder.setView(view)
            button.setOnClickListener{
                recreate()
            }
            builder.setTitle("Error")
            var dialog: android.app.AlertDialog =builder.create()
            dialog.show()
        }
    }

    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    private fun setup(){
        //Seteamos las variables
        signin = findViewById<Button>(R.id.signin)
        email = findViewById<EditText>(R.id.setEmail)
        pass1 = findViewById<EditText>(R.id.setPassword)
        pass2 = findViewById<EditText>(R.id.setPasswordConf)
        loading = findViewById(R.id.loadBar2)

        error = findViewById<TextView>(R.id .confirmation)

        signin.setOnClickListener{
            val emailText = email.text.toString()
            val pass1Text = pass1.text.toString()
            val pass2Text = pass2.text.toString()

            if(emailText.isNotEmpty() && pass1Text.isNotEmpty() && pass2Text.isNotEmpty()){
                if(pass1Text==pass2Text){
                    error.visibility=TextView.INVISIBLE
                    signup(emailText, pass1Text)

                }else{
                    error.visibility=TextView.VISIBLE
                }
            }else{
               showErrorMessage("Introduce the values")
            }

        }
    }

    fun showErrorMessage(message: String){
        var builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(message)
        builder.setPositiveButton("Aceptar", null)
        var dialog: android.app.AlertDialog =builder.create()
        dialog.show()
    }

    fun showCreateProfile1(){
        val createProfileIntent = Intent(this, CreateProfile1::class.java)
        startActivity(createProfileIntent)
    }

    override fun login() {

    }

    override fun loginGoogle() {

    }

    override fun signup(email: String, pass: String) {
        authPresenter?.signup(email, pass)
    }

    override fun verifyLogin() {

    }

    override fun exitoso(exito: Boolean, error: String?) {
        if(exito){
            showCreateProfile1()
        }else{
            showErrorMessage(error?:"")
        }
    }

    override fun getContext(): Context {
        return applicationContext
    }

    override fun setLoading(status: Boolean) {
            if(status){
                loading.visibility = View.VISIBLE
                email.isEnabled = false
                pass1.isEnabled = false
                pass2.isEnabled = false

                signin.visibility = View.INVISIBLE
            }else{
                loading.visibility = View.INVISIBLE
                email.isEnabled = true
                pass1.isEnabled = true
                pass2.isEnabled = true

                signin.visibility = View.VISIBLE
            }
    }
}