package com.moviles.workapp.model

import android.content.Context
import android.graphics.Bitmap
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.moviles.workapp.presenter.userPresenter
import java.io.ByteArrayOutputStream
import kotlin.math.max

class UserRepositoryImpl(private var userPresenter: userPresenter):UserRepository {

    private val context = userPresenter.getContext()

    private val email = "userEmail"
    private val name = "userName"
    private val age = "userAge"
    private val studyLevel = "userStudyLevel"
    private val phone = "userPhone"
    private val location = "userLocation"
    private val skills = "userSkill"
    private val configuration = "Configuracion"

    private var storage: FirebaseStorage = Firebase.storage

    private var fireAuth = FirebaseAuth.getInstance()

    override fun getUserAPI() {

        val uid = fireAuth.currentUser?.uid?:""
        var user:User? = getUserFromPreferences2()
        if (user?.email.isNullOrEmpty()){
            getUserFromFirebase(uid)
        }else{
            userPresenter.showUser(user, null)
        }
    }

    override fun setUserAPI(user:User) {
        val database = FirebaseDatabase.getInstance().getReference("Users")
        val email = fireAuth.currentUser?.email

        val uid = fireAuth.currentUser?.uid?:""

        user.email = email
        storeUserInPreferences(user)
        database.child(uid).setValue(user).addOnSuccessListener {
            val sharedPrefs = context.getSharedPreferences("userFile" ,Context.MODE_PRIVATE)
            var maxPrefs = sharedPrefs.getInt("Configuracion", 0)
            with(sharedPrefs.edit()) {
                putInt("Configuracion", max(1, maxPrefs))
                apply()
            }
            userPresenter.showUser(user, null)
        }.addOnFailureListener{
            userPresenter.showUser(null, it.localizedMessage)
        }
    }

    override fun uploadPicture(picture: Bitmap, uid: String) {

        val storageRef = storage.reference

        val spaceRef = storageRef.child("profileImages/$uid.jpeg")

        val baos = ByteArrayOutputStream()
        picture.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val uploadTask = spaceRef.putBytes(data)
        uploadTask.addOnFailureListener {
            userPresenter.finishedUploadPicture(false, it.localizedMessage)
        }.addOnSuccessListener { taskSnapshot ->
            userPresenter.finishedUploadPicture(true, null)
        }
    }

    override fun storeUserInPreferences(user: User){
        val sharedPrefs = context.getSharedPreferences("userFile", Context.MODE_PRIVATE)
        with(sharedPrefs.edit()) {
            //Aniado los valores necesarios
            putString(email, user.email)
            putString(name, user.name)
            putInt(age, user.age ?: 0)
            putString(studyLevel, user.studyLevel)
            putString(phone, user.phone)
            putString(location, user.location)

            var i = 0
            var skill = ""

            while (i < user.skills?.size ?: 0) {
                skill = user.skills?.get(i) ?: ""
                putString(skills + (i++), skill)
            }
            apply()
        }
    }

    private fun getUserFromFirebase(uid: String){
        var user: User? = null
        val database = FirebaseDatabase.getInstance().getReference("Users")
        database.child(uid).get().addOnSuccessListener {

            val child = it.child("skills")
            if (child.value != null){
                val skills: List<String> = it.child("skills").value as List<String>
                val phone: String = it.child("phone").value as String
                val name: String = it.child("name").value as String
                val loc: String = it.child("location").value as String
                val study: String = it.child("studyLevel").value as String
                val age: Long = it.child("age").value as Long
                val configuracion: Long = it.child("configuration").value as Long
                val email: String = it.child("email").value as String

                user = User(email, name, age.toInt(), study, phone, loc, skills, configuracion.toInt())
                storeUserInPreferences(user!!)
            }
            userPresenter.showUser(user, "show")
        }.addOnFailureListener{
            print(it.localizedMessage)
            userPresenter.showUser(user, "show")
        }
    }

    override fun getUserFromPreferences(){
        val uid = fireAuth.currentUser?.uid?:""
        var user:User? = getUserFromPreferences2()
        if (user?.email.isNullOrEmpty() && user?.name.isNullOrEmpty()
            && user?.age != -1 && user?.studyLevel.isNullOrEmpty()
            && user?.phone.isNullOrEmpty()
            && user?.skills?.get(0).isNullOrEmpty()){
                getUserFromFirebase(uid)
        }else{
            userPresenter.showUser(user, "show")
        }
    }

    private fun getUserFromPreferences2():User{
        val sharedPrefs = context.getSharedPreferences("userFile", Context.MODE_PRIVATE)
        //Get all the values from preferences
        val usemail = sharedPrefs.getString(email, "")
        val usname = sharedPrefs.getString(name, "")
        val usage = sharedPrefs.getInt(age, 0)
        val usstudyLevel = sharedPrefs.getString(studyLevel, "")
        val usphone = sharedPrefs.getString(phone, "")
        val uslocation = sharedPrefs.getString(location, "")
        val usconfiguration = sharedPrefs.getInt(configuration, 0)

        val userSkills = mutableListOf<String>()

        var i=0

        var skill = sharedPrefs.getString(skills + (i++), "")
        userSkills.add(skill!!)
        while (!skill.isNullOrBlank() && i<10){
            userSkills.add(skill!!)
            skill = sharedPrefs.getString(skills + (i++), "")
        }

        return User(usemail, usname, usage, usstudyLevel, usphone, uslocation, userSkills, usconfiguration)

    }

    override fun updateUserConfStatus(status: Int) {
        val uid = fireAuth.currentUser?.uid?:""
        val confStatus = FirebaseDatabase.getInstance().getReference("Users").child(uid).child("configuration")
        confStatus.setValue(status).addOnSuccessListener{
            val sharedPrefs = context.getSharedPreferences("userFile", Context.MODE_PRIVATE)
            with(sharedPrefs.edit()){
                putInt("Configuracion", status)
                apply()
            }
            userPresenter.showUser(null, null)
        }.addOnFailureListener{
            userPresenter.showUser(null, it.localizedMessage)
        }
    }

}