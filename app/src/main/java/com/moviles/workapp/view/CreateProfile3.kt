package com.moviles.workapp.view

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat.startActivityForResult
import coil.load
import coil.transform.CircleCropTransformation
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.moviles.workapp.R
import com.moviles.workapp.databinding.ActivityCreateProfile3Binding
import com.moviles.workapp.model.User
import com.moviles.workapp.presenter.userPresenter
import com.moviles.workapp.presenter.userPresenterImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.IOException

class CreateProfile3 : AppCompatActivity(), UserView{

    private var userPresenter: userPresenter? = null

    private lateinit var binding: ActivityCreateProfile3Binding
    private val CAMERA_REQUEST_CODE =1
    private val GALLERY_REQUEST_CODE =2

    private var picture: Bitmap? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityCreateProfile3Binding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        binding.btnCamera.setOnClickListener{
            cameraCheckPermission()
        }
        binding.btnGallery.setOnClickListener{
            galleryCheckPermission()
        }

        userPresenter= userPresenterImpl(this)


        binding.imageView12.setOnClickListener{
            val pictureDialog = AlertDialog.Builder(this)
            pictureDialog.setTitle("Select Action")
            val pictureDialogItem = arrayOf("Select photo from Gallery", "Capture photo from Camera")
            pictureDialog.setItems(pictureDialogItem){dialog, which ->
                when(which){
                    0 -> gallery()
                    1 -> camera()
                }
        }
            pictureDialog.show()
        }

        val button: Button = findViewById(R.id.buttonFinish)
        button.setOnClickListener {
            val uid: String = FirebaseAuth.getInstance().currentUser?.uid?:""
            var picture = picture
            uploadPicture(picture, uid)

            userPresenter?.updateUserConfStatus(3)
        }


        getUser()
    }

    private fun galleryCheckPermission(){
        Dexter.withContext(this).withPermission(
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ).withListener(object:PermissionListener{
            override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                gallery()
            }
            override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                Toast.makeText(
                    this@CreateProfile3,
                    "You have denied the storage permission to select image",
                    Toast.LENGTH_SHORT
                ).show()
                showRotationalDialogForPermission()
            }
            override fun onPermissionRationaleShouldBeShown(
                p0: com.karumi.dexter.listener.PermissionRequest?,
                p1: PermissionToken?
            ) {
                showRotationalDialogForPermission()
            }

        }).onSameThread().check()
    }

    private fun gallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,GALLERY_REQUEST_CODE)
    }

    private fun cameraCheckPermission(){
        Dexter.withContext(this).withPermissions(android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA).withListener(
                     object: MultiplePermissionsListener {
                         override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                             report?.let {
                                 if(report.areAllPermissionsGranted()){
                                     camera()
                                 }
                             }
                         }
                         override fun onPermissionRationaleShouldBeShown(
                             p0: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                             p1: PermissionToken?
                         ) {
                             showRotationalDialogForPermission()
                         }

                     }
        ).onSameThread().check()

    }
    private fun camera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        picture=null
        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                CAMERA_REQUEST_CODE->{
                    val bitmap = data?.extras?.get("data") as Bitmap
                    Glide.with(this)
                        .load(bitmap)
                        .fitCenter()
                        .placeholder(R.drawable.user)
                        .circleCrop()
                        .into(binding.imageView12)
                    picture = bitmap
                }
                GALLERY_REQUEST_CODE->{
                    val filePath = data?.data
                    try {
                        val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                        picture = bitmap
                        Glide.with(this)
                            .load(picture)
                            .fitCenter()
                            .placeholder(R.drawable.user)
                            .circleCrop()
                            .into(binding.imageView12)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
    private fun showRotationalDialogForPermission(){
        AlertDialog.Builder(this)
            .setMessage("It looks like you have turned off permissions required for this feature. It can be enable under App settings.")
            .setPositiveButton("Go to Settings"){_,_->
                try{
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri= Uri.fromParts("package",packageName,null)
                    intent.data=uri
                }catch (e: ActivityNotFoundException){
                    e.printStackTrace()
                }
            }
            .setNegativeButton("CANCEL"){dialog,_->
                dialog.dismiss()
            }.show()
    }

    override fun uploadPicture(picture: Bitmap?, uid: String){
        userPresenter?.uploadPicture(picture, uid)
    }

    override fun finishedUploadPicture(exitoso: Boolean, error: String?) {
        if(exitoso){
            showJobs()
        }
        else{
            showAlert(error?:"")
        }
    }

    override fun getContext(): Context {
        return applicationContext
    }

    override fun setLoading(status: Boolean){
        if(status){
            binding.load3.visibility = View.VISIBLE

        }else{
            binding.load3.visibility = View.INVISIBLE

        }
    }


    override fun showUser(user: User?, message: String?) {
        if (user != null){
            val name = user?.name
            val age = user?.age?:1
            val email = user?.email

            findViewById<TextView>(R.id.namePreview).text = name
            findViewById<TextView>(R.id.agePreview).text = age.toString()
            findViewById<TextView>(R.id.emailPreview).text = email
        }else if(user == null && message != null){
            showAlert(message?:"")
        }
    }

    private fun showAlert(message: String){
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(message)
        builder.setPositiveButton("Accept", null)
        val dialog: androidx.appcompat.app.AlertDialog =builder.create()
        dialog.show()
    }

    override fun getUser() {
        userPresenter?.getUser()
    }
    override fun onResume() {
        super.onResume()
        drawAlert()
    }

    private fun drawAlert(){
        if (checkForInternet(this)) {
            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            var builder = android.app.AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.noconnection,null)
            val button =view.findViewById<Button>(R.id.btnTryAgain)
            builder.setView(view)
            button.setOnClickListener{
                recreate()
            }
            builder.setTitle("Error")
            var dialog: android.app.AlertDialog =builder.create()
            dialog.show()
        }
    }

    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    fun showJobs(){
        val intent: Intent = Intent(this, FindJobs:: class.java)
        startActivity(intent)
    }


    override fun setUser() {
        TODO("Not yet implemented")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable("picture", picture)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        picture = savedInstanceState.getParcelable("picture")
        Glide.with(this)
            .load(picture)
            .fitCenter()
            .placeholder(R.drawable.user)
            .circleCrop()
            .into(binding.imageView12)
        super.onRestoreInstanceState(savedInstanceState)
    }

}