package com.moviles.workapp.view

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.moviles.workapp.R
import com.moviles.workapp.databinding.ActivityMainBinding
import com.moviles.workapp.model.Jobs
import com.moviles.workapp.presenter.AuthenticationPresenter
import com.moviles.workapp.presenter.AuthenticationPresenterImpl
import com.moviles.workapp.presenter.userPresenterImpl


class FindJobs : AppCompatActivity(), AuthenticationManager {

    private lateinit var autPresenter: AuthenticationPresenter

    private lateinit var jobsArrayList: ArrayList<Jobs>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_jobs)
        autPresenter = AuthenticationPresenterImpl(this)
        val button: Button = findViewById(R.id.logout)



        button.setOnClickListener {
            autPresenter.logout()
            val mainActivity = Intent(this, MainActivity::class.java)
            startActivity(mainActivity)
        }

        supportActionBar?.hide()
        drawAlert()

        val images = arrayOf(
            R.drawable.el_corral,
            R.drawable.buro,
            R.drawable.studentpicsquare
        )

        val description = arrayOf(
            "Mesero",
            "Sales",
            "Baby Sitter"
        )
        val names = arrayOf(
            "El Corral",
            "BURO",
            "Juan Daniel"
        )
        val salary = arrayOf(
            Integer(10000),
            Integer(400000),
            Integer(700000)
        )
        val time = arrayOf(
            "4 days",
            "1 month",
            "11 days"
        )

        jobsArrayList = ArrayList()

        var i = 0

        while (i<time.size){
            val job = Jobs(description[i], names[i], salary[i], time[i], images[i])
            jobsArrayList.add(job)
            i++
        }

        val listview = findViewById<ListView>(R.id.listitem)
        listview.adapter =JobAdapter(this, jobsArrayList)
        listview.setOnItemClickListener{ parent, view, position, id ->
            val name = names[position]
            val description = description[position]
            val salary = salary[position]
            val time = time[position]
            val image = images[position]

            val i = Intent(this, JobDetail::class.java)
            i.putExtra("name", name)
            i.putExtra("description", description)
            i.putExtra("salary", salary)
            i.putExtra("time", time)
            i.putExtra("image", image)

            startActivity(i)
        }
    }

    override fun onResume() {
        super.onResume()
        drawAlert()

    }


        private fun drawAlert(){
        if (checkForInternet(this)) {
            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            var builder = android.app.AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.noconnection,null)
            val button =view.findViewById<Button>(R.id.btnTryAgain)
            builder.setView(view)
            button.setOnClickListener{
                recreate()
            }
            builder.setTitle("Error")
            var dialog: android.app.AlertDialog =builder.create()
            dialog.show()
        }
    }

    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    override fun login() {
        TODO("Not yet implemented")
    }

    override fun loginGoogle() {
        TODO("Not yet implemented")
    }

    override fun signup(email: String, pass: String) {
        TODO("Not yet implemented")
    }

    override fun verifyLogin() {
        TODO("Not yet implemented")
    }

    override fun exitoso(exito: Boolean, error: String?) {
        TODO("Not yet implemented")
    }

    override fun getContext(): Context {
        return applicationContext
        TODO("Not yet implemented")
    }

    override fun setLoading(status: Boolean) {
        TODO("Not yet implemented")
    }


}