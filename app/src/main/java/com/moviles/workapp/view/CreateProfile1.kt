package com.moviles.workapp.view

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.moviles.workapp.R
import com.moviles.workapp.model.User
import com.moviles.workapp.presenter.userPresenter
import com.moviles.workapp.presenter.userPresenterImpl

class CreateProfile1 : AppCompatActivity(), UserView {

    private var userPresenter:userPresenter? = null

    private lateinit var nameText:EditText
    private lateinit var ageText:EditText
    private lateinit var studyLevelText:Spinner
    private lateinit var phoneText:EditText
    private lateinit var locationText:EditText
    private lateinit var loading: ProgressBar

    private lateinit var comunication:Switch
    private lateinit var autonomy:Switch
    private lateinit var listening:Switch
    private lateinit var publicSpeaking:Switch
    private lateinit var design:Switch
    private lateinit var teamWork:Switch
    private lateinit var leadership:Switch
    private lateinit var creativity:Switch

    private lateinit var numSkills: TextView

    private var skills: MutableList<String> = mutableListOf<String>()

    private lateinit var study: Array<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_profile1)
        supportActionBar?.hide()
        drawAlert()


        userPresenter=userPresenterImpl(this)

        study = resources.getStringArray(R.array.studyLevel)

        nameText = findViewById(R.id.name)
        ageText = findViewById(R.id.age)
        ageText.setText("0")
        studyLevelText= findViewById(R.id.spinnerStudy)
        phoneText=findViewById(R.id.phone)
        locationText=findViewById(R.id.location)

        loading = findViewById(R.id.load1)

        //set the skills
        comunication = findViewById(R.id.comunication)
        autonomy = findViewById(R.id.autonomy)
        listening = findViewById(R.id.Listening)
        publicSpeaking = findViewById(R.id.publicSpeaking)
        design = findViewById(R.id.design)
        teamWork = findViewById(R.id.teamWork)
        leadership = findViewById(R.id.Leadership)
        creativity = findViewById(R.id.creativity)

        numSkills = findViewById(R.id.numSkills)

        comunication.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(comunication, activated)
        }

        autonomy.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(autonomy, activated)
        }

        listening.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(listening, activated)
        }

        publicSpeaking.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(publicSpeaking, activated)
        }

        design.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(design, activated)
        }

        teamWork.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(teamWork, activated)
        }

        leadership.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(leadership, activated)
        }

        creativity.setOnCheckedChangeListener{buttonView: CompoundButton, activated: Boolean ->
            onSelect(creativity, activated)
        }

        setLoading(true)
        val button: Button = findViewById(R.id.buttonContinue)
        button.setOnClickListener {
            setUser()
        }

    }

    override fun onResume() {
        super.onResume()
        drawAlert()
        userPresenter?.getUserFromPreferences()

    }

    fun onSelect(skill: Switch, isActivated: Boolean){
        if(isActivated){
            if(skills.size < 4){
                skills.add(skill.text as String)
            }else{
                skill.isChecked = false
            }
        }else{
            skills.remove(skill.text)
        }

        numSkills.text = skills.size.toString() + "/4"
    }



    private fun drawAlert(){
        if (checkForInternet(this)) {
            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            var builder = android.app.AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.noconnection,null)
            val button =view.findViewById<Button>(R.id.btnTryAgain)
            builder.setView(view)
            button.setOnClickListener{
                recreate()
            }
            builder.setTitle("Error")
            var dialog: android.app.AlertDialog =builder.create()
            dialog.show()
        }
    }

    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    override fun onStop() {
        super.onStop()
        val name:String = nameText.text.toString()
        val age:Int = Integer.parseInt(ageText.text.toString())
        val studyLevel:String = studyLevelText.getItemAtPosition(studyLevelText.selectedItemPosition) as String
        val phone:String= phoneText.text.toString()
        val location:String=locationText.text.toString()
        val user = User(null, name, age, studyLevel, phone, location, skills, 1)

        userPresenter?.storeUserInPreferences(user)
    }

    override fun showUser(user: User?, message: String?) {
        if (user == null && message == null){
            showAlert(message?:"")
        }
        else if(user!=null){
            if (message == null){
                showCreateProfile2()
            }
            else {
                nameText.setText(user.name)
                ageText.setText(user.age.toString())
                studyLevelText.setSelection(study.binarySearch(user.studyLevel))
                phoneText.setText(user.phone)
                locationText.setText(user.location)

                for(skill in (user.skills?: listOf("")).iterator()){
                    if(skill == comunication.text.toString()){
                        comunication.isChecked = true
                    }
                    if(skill == autonomy.text.toString()){
                        autonomy.isChecked = true
                    }
                    if(skill == listening.text.toString()){
                        listening.isChecked = true
                    }
                    if(skill == publicSpeaking.text.toString()){
                        publicSpeaking.isChecked = true
                    }
                    if(skill == design.text.toString()){
                        design.isChecked = true
                    }
                    if(skill == teamWork.text.toString()){
                        teamWork.isChecked = true
                    }
                    if(skill == leadership.text.toString()){
                        leadership.isChecked = true
                    }
                    if(skill == creativity.text.toString()){
                        creativity.isChecked = true
                    }
                }

                numSkills.text = skills.size.toString() + "/4"
            }
        }
    }

    private fun showAlert(message: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(message)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog =builder.create()
        dialog.show()
    }

    override fun getUser() {
        TODO("Not yet implemented")
    }

    override fun setUser(){
        val name:String = nameText.text.toString()
        val age:Int = Integer.parseInt(ageText.text.toString())
        val studyLevel:String = studyLevelText.getItemAtPosition(studyLevelText.selectedItemPosition) as String
        val phone:String= phoneText.text.toString()
        val location:String=locationText.text.toString()


        val user = User(null, name, age, studyLevel, phone, location, skills, 1)



        userPresenter?.setUser(user)
    }
    override fun setLoading(status: Boolean){
        if(status){
            loading.visibility = View.VISIBLE

        }else{
            loading.visibility = View.INVISIBLE

        }
    }

    override fun uploadPicture(picture: Bitmap?, uid: String) {
        TODO("Not yet implemented")
    }

    override fun finishedUploadPicture(exitoso: Boolean, error: String?) {
        TODO("Not yet implemented")
    }

    override fun getContext():Context {
        return applicationContext
    }

    private fun showCreateProfile2(){
        val createProfileIntent = Intent(this, CreateProfile2::class.java)
        startActivity(createProfileIntent)
    }
}