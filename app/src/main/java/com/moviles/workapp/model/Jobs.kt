package com.moviles.workapp.model

public data class Jobs(val description: String, val name: String, val salary: Integer, val time: String, val image: Int) {
}