package com.moviles.workapp.model

import android.content.Context
import android.graphics.Bitmap
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.moviles.workapp.presenter.userPresenter

class UserInteractorImpl(var userPresenter: userPresenter): UserInteractor {

    private var userRepository:UserRepository = UserRepositoryImpl(userPresenter)

    override fun getUserAPI() {
        userRepository.getUserAPI()
    }

    override fun uploadPicture(picture: Bitmap, uid: String) {
        userRepository.uploadPicture(picture, uid)
    }

    override fun setUserAPI(user:User){
        userRepository.setUserAPI(user)
    }

    override fun storeUserInPreferences(user: User){
        userRepository.storeUserInPreferences(user)
    }

    override fun getUserFromPreferences(){
        userRepository.getUserFromPreferences()
    }

    override fun updateUserConfStatus(status: Int) {
        userRepository.updateUserConfStatus(status)
    }

}