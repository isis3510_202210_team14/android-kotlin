package com.moviles.workapp.presenter

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.moviles.workapp.model.AuthenticatorInteractor
import com.moviles.workapp.model.AuthenticatorInteractorImpl
import com.moviles.workapp.view.AuthenticationManager
import kotlinx.coroutines.*
import java.lang.Exception

class AuthenticationPresenterImpl(var authManager: AuthenticationManager): AuthenticationPresenter {

    private var authenticationInteractor: AuthenticatorInteractor = AuthenticatorInteractorImpl(this)

    override fun login(email: String, pass: String) {
        authManager.setLoading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                authenticationInteractor.login(email, pass)
            }catch (e: Exception){

            }
        }
    }

    override fun loginGoogle(task: Task<GoogleSignInAccount>) {
        authManager.setLoading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                authenticationInteractor.loginGoogle(task)
            }catch (e: Exception){

            }
        }
    }

    override fun signup(email: String, pass: String) {
        authManager.setLoading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                authenticationInteractor.signup(email, pass)
            }catch (e: Exception){

            }
        }
    }

    override fun verifyLogin() {
        GlobalScope.launch(Dispatchers.IO) {
            try{

                var status = authenticationInteractor.verifyLogin()
                GlobalScope.launch(Dispatchers.Main) {
                    authManager.setLoading(false)
                    if(status=="-1"){
                        authManager.exitoso(false, null)
                    }else{
                        authManager.exitoso(true, status)
                    }
                }
            }catch(e: Exception){
                GlobalScope.launch(Dispatchers.Main) {
                    authManager.exitoso(false, e.localizedMessage)
                }
            }
        }
    }

    override fun exitoso(exito: Boolean, error: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            authManager.setLoading(false)
            authManager.exitoso(exito, error)
        }
    }

    override fun getContext(): Context {
        return authManager.getContext()
    }

    override fun logout() {
        authenticationInteractor.logout()
    }
}