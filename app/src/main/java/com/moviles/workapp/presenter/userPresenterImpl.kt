package com.moviles.workapp.presenter

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import com.moviles.workapp.model.User
import com.moviles.workapp.model.UserInteractor
import com.moviles.workapp.model.UserInteractorImpl
import com.moviles.workapp.view.UserView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class userPresenterImpl(var userView: UserView): userPresenter {

    private var userInteractor: UserInteractor = UserInteractorImpl(this)

    override fun showUser(user:User?, message: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                userView.setLoading(false)
                userView.showUser(user, message)
            }catch (e: Exception){

            }
        }
    }

    override fun setUser(user: User) {
        userView.setLoading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                userInteractor.setUserAPI(user)
            }catch (e: Exception){

            }
        }
    }

    override fun getUser() {
        userView.setLoading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                userInteractor.getUserAPI()
            }catch (e: Exception){

            }
        }
    }

    override fun uploadPicture(picture: Bitmap?, uid: String) {
        userView.setLoading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                if(picture != null){
                    userInteractor.uploadPicture(picture, uid)
                    updateUserConfStatus(3)
                }else{
                    updateUserConfStatus(3)
                    finishedUploadPicture(true, "")
                }
            }catch (e: Exception){

            }
        }
    }

    override fun finishedUploadPicture(exitoso: Boolean, error:String?) {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                userView.setLoading(false)
                userView.finishedUploadPicture(exitoso, error)
            }catch (e: Exception){

            }
        }
    }

    override fun getContext(): Context {
        return userView.getContext()
    }

    override fun storeUserInPreferences(user: User){
        userInteractor.storeUserInPreferences(user)
    }

    override fun getUserFromPreferences() {
        userInteractor.getUserFromPreferences()
    }

    override fun updateUserConfStatus(status: Int) {
        userInteractor.updateUserConfStatus(status)
    }
}