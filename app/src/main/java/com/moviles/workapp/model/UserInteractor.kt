package com.moviles.workapp.model

import android.content.Context
import android.graphics.Bitmap

interface UserInteractor {

    fun setUserAPI(user: User)

    fun getUserAPI()

    fun uploadPicture(picture: Bitmap, uid: String)

    fun storeUserInPreferences(user: User)

    fun getUserFromPreferences()

    fun updateUserConfStatus(status: Int)
}