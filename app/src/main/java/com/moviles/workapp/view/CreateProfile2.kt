package com.moviles.workapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.OpenableColumns
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.moviles.workapp.R
import com.moviles.workapp.model.User
import com.moviles.workapp.presenter.userPresenter
import com.moviles.workapp.presenter.userPresenterImpl

private lateinit var pdfTextView: TextView
private lateinit var pdfUri: Uri

class CreateProfile2 : AppCompatActivity(), UserView{
    private var userPresenter: userPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_profile2)
        supportActionBar?.hide()
        drawAlert()
        val button: Button = findViewById(R.id.btnUploadFile)
        val btnFile: Button = findViewById(R.id.btnFile)

        btnFile.setOnClickListener{
            selectPdf()
        }

        pdfTextView = findViewById(R.id.selectFile)

        userPresenter= userPresenterImpl(this)
        button.setOnClickListener {
            userPresenter?.updateUserConfStatus(2)
        }
    }

    private fun selectPdf() {
        val pdfIntent = Intent(Intent.ACTION_GET_CONTENT)
        pdfIntent.type = "application/pdf"
        pdfIntent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(pdfIntent, 12)
    }

    // Override this method to allow you select an an image or a PDF
    @SuppressLint("Range")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            12 -> if (resultCode == RESULT_OK) {
                pdfUri = data?.data!!
                val uri: Uri = data?.data!!
                val uriString: String = uri.toString()
                var pdfName: String? = null
                if (uriString.startsWith("content://")) {
                    var myCursor: Cursor? = null
                    try {
                        // Setting the PDF to the TextView
                        myCursor = applicationContext!!.contentResolver.query(uri, null, null, null, null)
                        if (myCursor != null && myCursor.moveToFirst()) {
                            pdfName = myCursor.getString(myCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                            pdfTextView.text = pdfName
                        }
                    } finally {
                        myCursor?.close()
                    }
                }
            }
        }
    }

    override fun showUser(user: User?, message: String?) {
        if(message==null){
            val intent: Intent = Intent(this, CreateProfile3::class.java)
            startActivity(intent)
        }else{
            showAlert(message)
        }
    }

    override fun onResume() {
        super.onResume()
        drawAlert()
    }

    private fun drawAlert(){
        if (checkForInternet(this)) {
            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            var builder = android.app.AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.noconnection,null)
            val button =view.findViewById<Button>(R.id.btnTryAgain)
            builder.setView(view)
            button.setOnClickListener{
                recreate()
            }
            builder.setTitle("Error")
            var dialog: android.app.AlertDialog =builder.create()
            dialog.show()
        }
    }

    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }


    private fun showAlert(message: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(message)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog =builder.create()
        dialog.show()
    }

    override fun getUser() {

    }

    override fun setUser() {

    }

    override fun setLoading(state: Boolean) {

    }

    override fun uploadPicture(picture: Bitmap?, uid: String) {

    }

    override fun finishedUploadPicture(exitoso: Boolean, error: String?) {

    }

    override fun getContext(): Context {
        return applicationContext
    }
}