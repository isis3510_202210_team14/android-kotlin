package com.moviles.workapp.view

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import com.moviles.workapp.model.User

interface UserView {

    fun showUser(user: User?, message: String?)

    fun getUser()

    fun setUser()

    fun uploadPicture(picture: Bitmap?, uid: String)

    fun finishedUploadPicture(exitoso: Boolean, error: String?)

    fun getContext():Context

    fun setLoading(state: Boolean)

}