package com.moviles.workapp.model

import android.graphics.Bitmap

interface UserRepository {
    fun getUserAPI()

    fun setUserAPI(user:User)

    fun uploadPicture(picture: Bitmap, uid: String)

    fun storeUserInPreferences(user: User)

    fun getUserFromPreferences()

    fun updateUserConfStatus(status: Int)

}