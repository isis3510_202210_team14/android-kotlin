package com.moviles.workapp.presenter

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import com.moviles.workapp.model.User

interface userPresenter {

    fun showUser(user:User?, message: String?)

    fun setUser(user: User)

    fun getUser()

    fun uploadPicture(picture: Bitmap?, uid: String)

    fun finishedUploadPicture(exitoso: Boolean, error: String?)

    fun getContext():Context

    fun storeUserInPreferences(user: User)

    fun getUserFromPreferences()

    fun updateUserConfStatus(status: Int)
}