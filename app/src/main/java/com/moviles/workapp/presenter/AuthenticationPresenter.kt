package com.moviles.workapp.presenter

import android.app.Activity
import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task

interface AuthenticationPresenter {
    fun login(email: String, pass: String)

    fun loginGoogle(task: Task<GoogleSignInAccount>)

    fun signup(email: String, pass: String)

    fun verifyLogin()

    fun exitoso(exito: Boolean, error: String?)

    fun getContext(): Context

    fun logout()

}