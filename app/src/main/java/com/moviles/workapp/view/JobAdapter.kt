package com.moviles.workapp.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.moviles.workapp.R
import com.moviles.workapp.model.Jobs

class JobAdapter(val appContext: Context, private val jobsArray: ArrayList<Jobs>): ArrayAdapter<Jobs>(appContext,
    R.layout.list_item, jobsArray) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.list_item, null)

        val imageView = view.findViewById<ImageView>(R.id.profile)
        val company = view.findViewById<TextView>(R.id.companyName)
        val pos = view.findViewById<TextView>(R.id.position)
        val salary = view.findViewById<TextView>(R.id.salary)

        imageView.setImageResource(jobsArray[position].image)
        company.text = jobsArray[position].name
        pos.text = jobsArray[position].description
        salary.text = jobsArray[position].salary.toString()

        return view
    }
}